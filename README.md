# NextLOTR

Proyecto base realizado en Next.JS para explicar y desarrollar sus características, se trata de una web que muestra información de los personajes de El Señor de los Anillos

## Instalación

Necesitarás tener instalado Node(12.18.*) y npm

```bash
npm i
```

### Desarrollo

Si quieres editar y ver los cambios en directo 

```bash
npm run dev
```

### Producción

Para ver la versión de producción

```bash
npm run build
npm start
```