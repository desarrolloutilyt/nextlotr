import Personaje from './personaje';

import Personajes from '../../data/personajes.json';

const GridPersonajes = () => (
	<div className='container-md'>
		<div className='flex-s-st flex-wrap'>
			{Personajes.map((personaje, index) => (
				<Personaje key={index} personaje={personaje} />
			))}
		</div>
	</div>
);

export default GridPersonajes;
