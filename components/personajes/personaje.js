const Personaje = ({ personaje }) => (
	<div className='w-1/4 p-1'>
		<div className='flexcol-c-c p-1 rounded-lg shadow-xl'>
			<h3>{personaje.name}</h3>
		</div>
	</div>
);

export default Personaje;
