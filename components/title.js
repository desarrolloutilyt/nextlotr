const Title = () => (
	<div className='container-md'>
		<div className='flex-c-c text-center'>
			<h1 className='text-4xl'>Lista de Personajes de El Señor de los Anillos</h1>
		</div>
	</div>
);

export default Title;
