import '../styles/index.css';

function CustomApp({ Component, pageProps }) {
	return <Component {...pageProps} />;
}

export default CustomApp;
