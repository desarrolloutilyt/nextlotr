import Title from '../components/title';
import GridPersonajes from '../components/personajes/gridPersonajes';

const Home = () => (
	<>
		<Title />
		<GridPersonajes />
	</>
);

export default Home;
