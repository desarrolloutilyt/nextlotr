import Personajes from '../../data/personajes.json';

const Personaje = ({ personaje }) => (
	<div className='container-md'>
		<h1>{personaje.name}</h1>
		<p>Birth: {personaje.birth}</p>
		<p>Death: {personaje.death}</p>
	</div>
);

export const getStaticPaths = async () => {
	const paths = Personajes.map((personaje, index) => '/personajes/' + index.toString());

	return { paths, fallback: false };
};

export const getStaticProps = async ({ params }) => {
	const personaje = Personajes[parseInt(params.personaje)];

	return {
		props: { personaje },
	};
};

export default Personaje;
